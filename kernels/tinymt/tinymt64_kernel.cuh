#ifndef TINYMT64_KERNEL_CUH
#define TINYMT64_KERNEL_CUH
/**
 * @file tinymt64_kernel.cuh
 *
 * @brief CUDA implementation of TinyMT64.
 *
 * This is CUDA implementation of TinyMT64 pseudo-random number generator.
 *
 * @author Mutsuo Saito (Hiroshima University)
 * @author Makoto Matsumoto (The University of Tokyo)
 *
 * Copyright (C) 2011 Mutsuo Saito, Makoto Matsumoto,
 * Hiroshima University and The University of Tokyo.
 * All rights reserved.
 *
 * The 3-clause BSD License is applied to this software, see LICENSE.txt
 */
#include <cuda.h>
#include <stdint.h>
/* =====================================
   DEFINITIONS FOR USERS
   ===================================== */
/**
 * TinyMT structure
 * mat1, mat2, tmat must be set from tinymt64dc output before initialize.
 *
 * The internal state is separated to four 32 bit unsigned integers,
 * for the purpose of speed up in CUDA environment, but the effect of
 * the separation is small.
 */
typedef struct TINYMT64_STATUS_T {
    uint32_t S0H;
    uint32_t S0L;
    uint32_t S1H;
    uint32_t S1L;
    uint32_t mat1;
    uint32_t mat2;
    uint64_t tmat;
} tinymt64_status_t;

/**
 * Initialize TinyMT structure by seed and parameters.
 *
 * This function must be called before tinymt64_uint64(),
 * tinymt64_double(), tinymt64_double12().
 * mat1, mat2, tmat in tinymt must be set before this call.
 *
 * @param tinymt TinyMT structure to be initialized.
 * @param seed seed of randomness.
 */
__device__ void tinymt64_init(tinymt64_status_t* tinymt, uint64_t seed);

/**
 * Generate 64bit unsigned integer r (0 <= r < 2<sup>64</sup>)
 *
 * @param tinymt TinyMT structure
 * @return 64bit unsigned integer
 */
__device__ uint64_t tinymt64_uint64(tinymt64_status_t * tinymt);

/**
 * Generate double precision floating point number r (0.0 <= r < 1.0)
 *
 * @param tinymt TinyMT structure
 * @return double precision floating point number
 */
__device__ double tinymt64_double(tinymt64_status_t * tinymt);

/**
 * Generate double precision floating point number r (1.0 <= r < 2.0).
 *
 * This function may little bit faster than tinymt64_double().
 *
 * @param tinymt TinyMT structure
 * @return double precision floating point number
 */
__device__ double tinymt64_double12(tinymt64_status_t * tinymt);

/* =====================================
   DEFINITIONS FOR INTERNAL USE
   ===================================== */
#define TINYMT64_SHIFT0 12
#define TINYMT64_SHIFT1 11
#define TINYMT64_MIN_LOOP 8
#define TINYMT64_MASK UINT64_C(0x7fffffffffffffff)
#define TINYMT64_MASK_HIGH 0x7fffffffU
#define TINYMT64_DOUBLE_MASK UINT64_C(0x3ff0000000000000)

__device__ void tinymt64_next(tinymt64_status_t * tinymt);
__device__ uint64_t tinymt64_temper(tinymt64_status_t * tinymt);
/* =====================================
   FUNCTIONS
   ===================================== */
/**
 * The state transition function.
 * @param tinymt the internal state.
 */
__device__ void tinymt64_next(tinymt64_status_t * tinymt)
{
    uint32_t xh = tinymt->S0H & TINYMT64_MASK_HIGH;
    uint32_t xl = tinymt->S0L;
    xh ^= tinymt->S1H;
    xl ^= tinymt->S1L;
    xh ^= xh << TINYMT64_SHIFT0;
    xh ^= xl >> (32 - TINYMT64_SHIFT0);
    xl ^= xl << TINYMT64_SHIFT0;
    xl ^= xh;
    xh ^= xl;
    xh ^= xh << TINYMT64_SHIFT1;
    xh ^= xl >> (32 - TINYMT64_SHIFT1);
    xl ^= xl << TINYMT64_SHIFT1;
    tinymt->S0H = tinymt->S1H;
    tinymt->S0L = tinymt->S1L;
    tinymt->S1H = xh;
    tinymt->S1L = xl;
    if (xl & 1) {
	tinymt->S0L ^= tinymt->mat1;
	tinymt->S1H ^= tinymt->mat2;
    } else {
    }
}

/**
 * The tempering function.
 *
 * This function improves the equidistribution property of
 * outputs.
 * @param tinymt the internal state.
 * @return tempered output
 */
__device__ uint64_t tinymt64_temper(tinymt64_status_t * tinymt)
{
    uint64_t t = tinymt->S0H + tinymt->S1H;
    t = t << 32;
    t += tinymt->S0L + tinymt->S1L;
    t ^= tinymt->S0L >> 8;
    t ^= (uint64_t)tinymt->S0H << 24;
    if (t & 1) {
	t ^= tinymt->tmat;
    }
    return t;
}

__device__ uint64_t tinymt64_uint64(tinymt64_status_t * tinymt)
{
    tinymt64_next(tinymt);
    return tinymt64_temper(tinymt);
}

__device__ double tinymt64_double12(tinymt64_status_t * tinymt)
{
    tinymt64_next(tinymt);
    uint64_t t = tinymt64_temper(tinymt);
    t = t >> 12;
    t ^= TINYMT64_DOUBLE_MASK;
    return __longlong_as_double(t);
}

__device__ double tinymt64_double(tinymt64_status_t * tinymt)
{
    return tinymt64_double12(tinymt) - 1.0;
}

__device__ void tinymt64_init(tinymt64_status_t* tinymt,
			      uint64_t seed)
{
    uint64_t status[2];

    status[0] = ((uint64_t)tinymt->S0H << 32) | tinymt->S0L;
    status[1] = ((uint64_t)tinymt->S1H << 32) | tinymt->S1L;

    status[0] = seed ^ ((uint64_t)tinymt->mat1 << 32);
    status[1] = tinymt->mat2 ^ tinymt->tmat;
    for (int i = 1; i < TINYMT64_MIN_LOOP; i++) {
        status[i & 1] ^= i + 6364136223846793005ULL
            * (status[(i - 1) & 1] ^ (status[(i - 1) & 1] >> 62));
    }
    if ((status[0] & TINYMT64_MASK) == 0 &&
	status[1] == 0) {
	status[0] = 'T';
	status[1] = 'M';
    }
    tinymt->S0H = status[0] >> 32;
    tinymt->S0L = status[0];
    tinymt->S1H = status[1] >> 32;
    tinymt->S1L = status[1];
}

#undef TINYMT64_SHIFT0
#undef TINYMT64_SHIFT1
#undef TINYMT64_MIN_LOOP
#undef TINYMT64_MASK
#undef TINYMT64_MASK_HIGH
#undef TINYMT64_DOUBLE_MASK

#endif
