#ifndef TINYMT_STATUS_CUH
#define TINYMT_STATUS_CUH

#include <stdint.h>

// TYS: Why did I do this? So we can include this in the host as well as device.

/**
 * TinyMT structure
 * mat1, mat2, tmat must be set from tinymt32dc output before initialize
 */
typedef struct TINYMT32_STATUS_T {
    uint32_t status[4];
    uint32_t mat1;
    uint32_t mat2;
    uint32_t tmat;
} tinymt32_status_t;

#endif

