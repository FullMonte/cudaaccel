#pragma once

#include "FullMonteCUDATypes.h"

#include "kernels/full_monte_device.cuh"

#include <vector>

using namespace std;

/**
 * A lightweight API for interacting with the CUDA kernels.
 * Exposes a few high level methods to run the FullMonte kernel.
 *
 * In general the flow would look like this:
 * 
 * CudaAccel dev;
 * dev.init(<data from host>)
 * dev.launch()                 <-- do this in a loop if the number of packets exceeds 'maxPacketsPossible'
 * dev.sync()
 * copy(dev.tetraEnergy_d)
 * dev.destroy()
 *
 */
class CUDAAccel {
public:
    CUDAAccel();
    ~CUDAAccel();

    bool init(
        const unsigned                  seed,
        const unsigned                  Nstep_max,
        const unsigned                  Nhit_max,
        const unsigned                  numFaces,
        const float                     wmin,
        const float                     prwin,
        const vector<CUDA::Material>    &materials,
        const vector<CUDA::Tetra>       &tetras,
        const bool                      copyTetras
    );
    bool launch(const vector<CUDA::LaunchPacket>& launched_packets);
    void sync();
    void destroy();

    ////////////////////////////////////////////////////////
    // Used to copy from the accelerator back to the host
    void copyTetraEnergy(vector<double>& tetraEnergy);
    void copyTetraEnergy(vector<float>& tetraEnergy);
    
    void copyDirectedSurfaceEnergy(vector<double>& withNormal, vector<double>& antiNormal);
    void copyDirectedSurfaceEnergy(vector<float>& withNormal, vector<float>& antiNormal);

    void copySurfaceExitEnergy(vector<double>& surfaceExitEnergy); 
    void copySurfaceExitEnergy(vector<float>& surfaceExitEnergy); 
    ////////////////////////////////////////////////////////

    void changeDevice(int devId);
    int currentDeviceID();
    unsigned int maxPacketsPossible();
    size_t maxTetraEstimate(unsigned int packetCountHint=0);
    bool isDeviceCapable();
 
    static bool isDeviceCapable(int devId);
    static void listDevices();
    
    // whether to score volume events
    bool scoreVolume=true;

    // whether to score directed surface enter/exit events
    bool scoreDirectedSurface=false;

    // whether to score surface (of the mesh) exit events
    bool scoreSurfaceExit=false;

private:
    // the CUDA device ID (of the GPU)
    int                 devId;

    // per GPU device constants
    unsigned int        maxThreadsPerBlock, prefThreadsPerBlock, maxBlocks;

    // track the last number of packets simulated
    unsigned int        lastPacketCount;

    // FullMonte kernel variables
    unsigned            seed, NstepMax, NhitMax;
    float               wMin, prWin;
    unsigned int        materialsCount, tetrasCount, facesCount;
    CUDA::Tetra*        tetras_d;
    CUDA::LaunchPacket* launchedPackets_d;
    tinymt32_status_t*  rngState_d;

    // accumulators
    double*             tetraEnergy_d;
    double*             meshExitEnergy_d;
    double*             faceEnergy1_d;
    double*             faceEnergy2_d;
};

